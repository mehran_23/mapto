package com.selectmap.app.model;

import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.selectmap.app.data.databases.preferences.SharePref;
import com.selectmap.app.inteface.LocationContract;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class LocationModel implements LocationContract.Model {
    private SharePref sharePref;
    private Gson gson;
    private final String LOCATION_KEY = "location";
    private static MutableLiveData<LatLng> latLngMutableLiveData=new MutableLiveData<>();

    public LocationModel(SharePref sharePref, Gson gson) {
        this.sharePref = sharePref;
        this.gson = gson;
    }

    @Override
    public void saveLocation(LatLng latLng) {
        List<LatLng> latLngList = loadLocation();

        if (latLngList == null)
            latLngList = new ArrayList<>();

        latLngList.add(latLng);
        String toJson = gson.toJson(latLngList);
        SharePref.Instance().put(LOCATION_KEY, toJson);

        latLngMutableLiveData.postValue(latLng);
    }

    @Override
    public List<LatLng> loadLocation() {
        String json = sharePref.get(LOCATION_KEY, "");
        Type listType = new TypeToken<List<LatLng>>() {
        }.getType();

        List<LatLng> latLngList = gson.fromJson(json, listType);

        if (latLngList == null) return new ArrayList<>();

        return latLngList;
    }

    public MutableLiveData<LatLng> getLatLngMutableLiveData() {
        return latLngMutableLiveData;
    }
}
