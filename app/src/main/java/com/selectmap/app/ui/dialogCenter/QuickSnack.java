package com.selectmap.app.ui.dialogCenter;

import android.app.Activity;

import com.google.android.material.snackbar.Snackbar;
import com.selectmap.app.utils.TextEditor;


public class QuickSnack {
    public QuickSnack(Activity activity, String message) {
        Snackbar.make(activity.findViewById(android.R.id.content)
                , TextEditor.changeTextColor(message, android.R.color.white)
                , Snackbar.LENGTH_INDEFINITE).show();
    }

    public QuickSnack(Activity activity, int idMessage) {
        Snackbar.make(activity.findViewById(android.R.id.content)
                , TextEditor.changeTextColor(activity.getString(idMessage)
                        , android.R.color.white), Snackbar.LENGTH_INDEFINITE).show();
    }
}