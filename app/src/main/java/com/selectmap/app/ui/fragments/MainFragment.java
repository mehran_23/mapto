package com.selectmap.app.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.selectmap.app.R;
import com.selectmap.app.constants.Phrase;
import com.selectmap.app.databinding.MainFragmentBinding;
import com.selectmap.app.materialBottomNavigation.MeowBottomNavigation;
import com.selectmap.app.ui.adapter.MainViewPagerAdapter;
import com.selectmap.app.utils.LocationUtils;


public class MainFragment extends BaseFragment {

    private MainFragmentBinding binding;
    public static int Title = R.string.Title_post_fragment;
    public static String Tag = "mapto: " + MainFragment.class.getSimpleName();
    private static MainFragment fragment;

    public static MainFragment newInstance() {
        if (fragment == null)
            fragment = new MainFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = MainFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpViewPager();

        binding.toolbarChatListFragment.setTitle(getString(R.string.app_name));

        MeowBottomNavigation bottomNavigation = binding.bottomNavigation;
        bottomNavigation.add(createMenu(Phrase.id_bottommenu_normal, R.drawable.ic_bottommenu_normal, ""));
        bottomNavigation.add(createMenu(Phrase.id_bottommenu_hybrid, R.drawable.ic_bottommenu_hybrid, ""));
        bottomNavigation.add(createMenu(Phrase.id_bottommenu_satelite, R.drawable.ic_bottommenu_satelite, ""));
        bottomNavigation.add(createMenu(Phrase.id_bottommenu_bookmark, R.drawable.ic_bottommeu_bookmark, ""));
        bottomNavigation.show(Phrase.id_bottommenu_normal, true);

        bottomNavigation.setOnClickMenuListener(model -> {
            switch (model.getId()) {
                case Phrase.id_bottommenu_normal:
                    changeCurrentFragment(0);
                    break;

                case Phrase.id_bottommenu_hybrid:
                    changeCurrentFragment(1);

                    break;

                case Phrase.id_bottommenu_satelite:
                    changeCurrentFragment(2);
                    break;

                case Phrase.id_bottommenu_bookmark:
                    changeCurrentFragment(3);
                    break;
            }
            return null;
        });

//        LocationUtils.statusCheck(getActivity());
    }

    public void changeCurrentFragment(int position) {
        if (binding.pager.getAdapter() != null && binding.pager.getChildCount() > position)
            binding.pager.setCurrentItem(position, true);
    }

    public void changeCurrentFragment(int position, boolean updateBottomNavigation) {
        if (binding.pager.getAdapter() != null && binding.pager.getChildCount() > position)
            binding.pager.setCurrentItem(position, true);

        if (updateBottomNavigation)
            binding.bottomNavigation.show(Phrase.id_bottommenu_bookmark, true);
    }

    private void setUpViewPager() {
        MainViewPagerAdapter adapter = new MainViewPagerAdapter(getChildFragmentManager());
        binding.pager.setAdapter(adapter);
        binding.pager.setOffscreenPageLimit(4);

        binding.pager.setCurrentItem(0, true);
        LocationUtils.statusCheck(getActivity());
    }

    private MeowBottomNavigation.Model createMenu(int idBottomMenu, int icBottomMeu, String count) {
        MeowBottomNavigation.Model model = new MeowBottomNavigation.Model(idBottomMenu, icBottomMeu);
        if (count != null && !count.equals("")) model.setCount(count);
        return model;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }

}
