package com.selectmap.app.ui.dialogCenter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.selectmap.app.R;

import static androidx.core.content.ContextCompat.getColor;

public class DialogCollection {
    private Context context;
    private OnDialogOptionSelected onDialogOptionSelected;
    private String title;
    private String description;
    private Integer icon;
    private String positiveBtnText;
    private String negativeBtnText;
    private String naturalBtnText;
    private static AlertDialog alertDialog;
    private int REQUEST_CODE;

    public DialogCollection(Context context,
                            OnDialogOptionSelected onDialogOptionSelected,
                            String title,
                            String description,
                            int REQUEST_CODE) {
        this.context = context;
        this.onDialogOptionSelected = onDialogOptionSelected;
        this.title = title;
        this.description = description;
        this.REQUEST_CODE = REQUEST_CODE;
    }

    public DialogCollection setIcon(int icon) {
        this.icon = icon;
        return this;
    }

    public DialogCollection setPositiveBtnText(String positiveBtnText) {
        this.positiveBtnText = positiveBtnText;
        return this;
    }

    public DialogCollection setNegativeBtnText(String negativeBtnText) {
        this.negativeBtnText = negativeBtnText;
        return this;
    }

    public DialogCollection setNaturalBtnText(String naturalBtnText) {
        this.naturalBtnText = naturalBtnText;
        return this;
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context
                , R.style.CustomMaterialAlertDialogBodyTextStyle).
                setTitle(getTitle())
                .setMessage(description);

        if (getIcon() != null)
            builder.setIcon(icon);

        if (getPositiveBtnText() != null)
            builder.setPositiveButton(getPositiveBtnText(), (dialog, which) ->
                    onDialogOptionSelected.onAcceptAlertDialog(REQUEST_CODE));

        if (getNegativeBtnText() != null)
            builder.setNegativeButton(getNegativeBtnText(), (dialog, which) ->
                    onDialogOptionSelected.onCancelAlertDialog());

        if (getNaturalBtnText() != null)
            builder.setNeutralButton(getNaturalBtnText(), (dialog, which) ->
                    onDialogOptionSelected.onNaturalAlertDialog());

        builder.setOnDismissListener(dialog ->
                onDialogOptionSelected.onDismissAlertDialog());

        builder.show();
    }

    public Context getContext() {
        return context;
    }

    public OnDialogOptionSelected getOnDialogOptionSelected() {
        return onDialogOptionSelected;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getIcon() {
        return icon;
    }

    public String getPositiveBtnText() {
        return positiveBtnText;
    }

    public String getNegativeBtnText() {
        return negativeBtnText;
    }

    public String getNaturalBtnText() {
        return naturalBtnText;
    }

    public interface OnDialogOptionSelected {
        void onAcceptAlertDialog(int requestCode);

        void onCancelAlertDialog();

        void onNaturalAlertDialog();

        void onDismissAlertDialog();
    }

    public static void showConnectionErrorDialog(Activity activity) {
        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.connection_error, null);
        ((ImageView) view.findViewById(R.id.img)).setImageResource(R.drawable.img_internet_error);
        ((TextView) view.findViewById(R.id.txt)).setText(R.string.reconnect_error);
        alertDialog = new AlertDialog.Builder(activity).setCancelable(false)
                .setView(view)
                .setPositiveButton("خروج"
                        , (dialog, which) -> activity.finishAffinity())

                .create();

        alertDialog.setOnShowListener(dialog -> {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getColor(activity, R.color.errorColor));
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getColor(activity, R.color.successColor));

        });

        alertDialog.show();

    }

    public static void showConfirmDialog(Activity activity, String msg,View.OnClickListener onClickListener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialogbox_confirm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView text = (TextView) dialog.findViewById(R.id.body);
        text.setText(msg);

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(v -> dialog.dismiss());

        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(v -> {
            onClickListener.onClick(v);
            dialog.cancel();
        });

        dialog.show();
    }


    public static void dismissDialogs() {
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }
}
