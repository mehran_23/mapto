package com.selectmap.app.ui.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.selectmap.app.App;
import com.selectmap.app.data.databases.preferences.SharePref;
import com.selectmap.app.ui.fragments.BaseFragment;
import com.selectmap.app.utils.ConnectionUtil;

import java.util.Locale;

import static com.selectmap.app.constants.ConnectionEnum.CONNECTION_DIALOG;
import static com.selectmap.app.constants.ConnectionEnum.CONNECTION_TOAST_FINISH;

public class BaseActivity extends AppCompatActivity
        implements BaseFragment.FragmentNavigation, BaseFragment.ChangeActivity {

    private Configuration configuration;
    private Locale myLocale;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout drawerLayout;
    private int viewPagerId;
    public static boolean isActivityStarted = false;

    protected void onStart() {
        super.onStart();
        setLocal();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setLocal();
    }

    private void setLocal() {
        int lang = SharePref.Instance().get("lang", 0);
        switch (lang) {
            case 0:
                myLocale = new Locale("fa");
                break;

            case 1:
                myLocale = new Locale("ar");
                break;

            case 2:
                myLocale = new Locale("en");
                break;

            default:
                myLocale = new Locale("fa");
                break;
        }
        Locale.setDefault(myLocale);
        configuration = new Configuration();
        configuration.locale = myLocale;
        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
    }

    @Override
    protected void onResume() {
        super.onResume();
        setLocal();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getLatestFragment().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void replaceFragment(BaseFragment fragment, String tag) {
//        if (!ConnectionUtil.isOnlineWithAlarm(this, CONNECTION_TOAST))
//            return;

        try {
            fragment.setFragmentNavigation(this);
            fragment.setChangeActivity(this);
            FragmentManager fragmentManager = getSupportFragmentManager();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(viewPagerId, fragment, tag).commit();
            transaction.addToBackStack(tag);
        } catch (Exception e) {
        }

    }

    @Override
    public void onFinish(Bundle bundle, String tag) {

    }

    @Override
    public void toggleDrawer() {
        if (drawerLayout == null) return;

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void startNextActivity(Class aClass, boolean isFinish) {
        startActivity(new Intent(BaseActivity.this, aClass));

        if (isFinish)
            finishAffinity();
    }

    @Override
    public void startNextActivity(Intent intent, boolean isFinish) {
        startActivity(intent);

        if (isFinish)
            finishAffinity();
    }

    public Fragment getLatestFragment() {
        String fragmentTag = getSupportFragmentManager()
                .getBackStackEntryAt(getSupportFragmentManager()
                        .getBackStackEntryCount() - 1).getName();
        return getSupportFragmentManager().findFragmentByTag(fragmentTag);
    }

    public void setmDrawerToggle(ActionBarDrawerToggle mDrawerToggle) {
        this.mDrawerToggle = mDrawerToggle;
    }

    public void setDrawerLayout(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
    }

    public void setViewPagerId(int viewPagerId) {
        this.viewPagerId = viewPagerId;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void checkConnection() {
        ConnectionUtil.isOnlineWithAlarm(this, CONNECTION_TOAST_FINISH);
    }

    protected boolean checkConnectionWithAlarm() {
        return ConnectionUtil.isOnlineWithAlarm(this, CONNECTION_DIALOG);
    }

    public boolean isActivityStarted() {
        return isActivityStarted;
    }

    public boolean setActivityStarted(boolean activityStarted) {
        isActivityStarted = activityStarted;
        return isActivityStarted;
    }

}
