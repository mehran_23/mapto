package com.selectmap.app.ui.dialogCenter;

import android.widget.Toast;

import com.selectmap.app.App;
import com.selectmap.app.R;

public class QuickToast {
    public QuickToast(String message) {
        Toast.makeText(App.Companion.applicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public QuickToast(int idMessage) {
        Toast.makeText(App.Companion.applicationContext(), App.Companion.applicationContext().getString(idMessage), Toast.LENGTH_SHORT).show();
    }

    public static void ErrorConnection() {
        Toast.makeText(App.Companion.applicationContext(), App.Companion.applicationContext().getString(R.string.error_in_connection), Toast.LENGTH_SHORT).show();
    }

    public static void ErrorInternetConnection() {
        Toast.makeText(App.Companion.applicationContext(), App.Companion.applicationContext().getString(R.string.error_in_internet_connection), Toast.LENGTH_LONG).show();
    }

}