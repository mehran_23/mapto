package com.selectmap.app.ui.activities.launcher


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.selectmap.app.R
import com.selectmap.app.databinding.ActivitySplashBinding
import com.selectmap.app.ui.activities.BaseActivity
import com.selectmap.app.ui.activities.MainActivity
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class SplashActivity : BaseActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: SplashViewModelFactory by instance()
    private lateinit var binding: ActivitySplashBinding;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                , WindowManager.LayoutParams.FLAG_FULLSCREEN)

        binding = ActivitySplashBinding.inflate(layoutInflater);
        setContentView(binding.root)

        val viewModel = ViewModelProviders.of(this, factory).get(SplashViewModel::class.java)
        viewModel.setFirstArrivalIfFlase()

        loadGif(R.drawable.gif_splash)
        scaleAnimation(0f, 0f, 1f, 1f, 500, 300)
        lazyStartActivity(5000)
    }


    fun scaleAnimation(
            fromX: Float, fromY: Float, toX: Float, toY: Float
            , delay: Long, duration: Long
    ) {
        val scaleGrow = ScaleAnimation(
                fromX,
                toX,
                fromY,
                toY,
                Animation.RELATIVE_TO_SELF,
                0.5.toFloat(),
                Animation.RELATIVE_TO_SELF,
                0.5.toFloat()
        )
        scaleGrow.startOffset = delay
        scaleGrow.duration = duration
        scaleGrow.fillAfter = true
        binding.imgTitle.startAnimation(scaleGrow)
    }

    fun loadGif(gifId: Int) {
        Glide.with(this)
                .load(gifId)
                .into(binding.imgLogo)
    }

    fun lazyStartActivity(delay: Long) {

        Handler().postDelayed(
                {
                    startActivity(Intent(this, MainActivity::class.java))
                    overridePendingTransition(R.anim.top_out, R.anim.bottom_in)

                }, delay
        )
    }
}
