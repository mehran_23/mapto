package com.selectmap.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;


public abstract class BaseFragment extends Fragment {
    private FragmentNavigation fragmentNavigation;
    private ChangeActivity changeActivity;

    public FragmentNavigation getFragmentNavigation() {
        if (fragmentNavigation != null)
            return fragmentNavigation;

        return (FragmentNavigation) getActivity();
    }

    public void setFragmentNavigation(FragmentNavigation fragmentNavigation) {
        this.fragmentNavigation = fragmentNavigation;
    }

    public ChangeActivity getChangeActivity() {
        if (changeActivity != null)
            return changeActivity;

        return (ChangeActivity) getActivity();
    }

    public void setChangeActivity(ChangeActivity changeActivity) {
        this.changeActivity = changeActivity;
    }

    public abstract String getName();

    public interface ChangeActivity {
        void startNextActivity(Class aClass, boolean isFinish);

        void startNextActivity(Intent intent, boolean isFinish);
    }


    public interface FragmentNavigation {
        void replaceFragment(BaseFragment fragment, String Tag);

        void onFinish(Bundle bundle, String tag);

        void toggleDrawer();
    }

}
