package com.selectmap.app.ui.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.selectmap.app.R;
import com.selectmap.app.databinding.StreetFragmentBinding;
import com.selectmap.app.inteface.StreetFragmentContract;
import com.selectmap.app.presenter.StreetFragmentPresenter;
import com.selectmap.app.ui.dialogCenter.DialogCollection;
import com.selectmap.app.ui.dialogCenter.QuickToast;
import com.selectmap.app.ui.dialogCenter.WaitProgressDialog;
import com.selectmap.app.utils.PlayServicesUtil;

import java.util.List;

public class StreetFragment extends BaseFragment implements StreetFragmentContract.View
        , View.OnClickListener, OnMapReadyCallback {

    private static final String POSITION_KEY = "position";
    private StreetFragmentPresenter presenter;
    private StreetFragmentBinding binding;
    public static String Tag = "yaghin: " + StreetFragment.class.getSimpleName();

    private GoogleMap googleMap;
    private final int CAMERA_ZOOM = 10;

    public static synchronized StreetFragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(POSITION_KEY, position);
        StreetFragment fragment = new StreetFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = StreetFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new StreetFragmentPresenter();
        presenter.attach(this);


        if (googleMap != null) {
            switch (getArguments().getInt(POSITION_KEY)) {
                case 0:
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.mapstyle));

                    break;

                case 1:
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.dark_mapstyle));

                    break;

                case 2:
                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.mapstyle));

                    break;

                case 3:
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.mapstyle));

                    break;

                default:
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    break;
            }
        }

        if (!PlayServicesUtil.isAvailable(getContext())) {
            showMessage("خدمات گوگل پلی برای این دستگاه غیر فعال می باشد.");
            getActivity().onBackPressed();
        } else {

            MapsInitializer.initialize(getContext());

            binding.map.onCreate(null);
            binding.map.getMapAsync(StreetFragment.this::onMapReady);
            binding.map.onStart();
        }

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void replaceHomeFragment() {
        getActivity().onBackPressed();
    }

    @Override
    public void showMessage(String message) {
        new QuickToast(message);
    }

    @Override
    public void showMessage(int message) {
        new QuickToast(message);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detach();
        binding = null;
    }

    @Override
    public void showProgressDialog() {
        WaitProgressDialog.showProgressDialog(getActivity());
    }

    @Override
    public void hideProgressDialog() {
        WaitProgressDialog.hideProgressDialog();
    }

    @Override
    public void onFinish() {
        getActivity().onBackPressed();
    }

    @Override
    public void showConfirmDialog(View.OnClickListener onClickListener) {
        DialogCollection.showConfirmDialog(getActivity(),
                "آیا تمایل به اضافه کردن این مکان به لیست مورد علاقه ها دارید ؟"
                , onClickListener);
    }

    @Override
    public void displayFavoritLocation(List<LatLng> loadLocation) {
        if (loadLocation != null && loadLocation.size() > 0 &&
                googleMap != null && getArguments().getInt(POSITION_KEY) == 3) {
            for (LatLng latLng : loadLocation)
                displayMarker(latLng, "");

            CameraUpdate coordinate = CameraUpdateFactory.newLatLngZoom(loadLocation.get(loadLocation.size() - 1), CAMERA_ZOOM);
            googleMap.animateCamera(coordinate);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        switch (getArguments().getInt(POSITION_KEY)) {
            case 0:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.mapstyle));

                break;

            case 1:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.dark_mapstyle));

                break;

            case 2:
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.mapstyle));

                break;

            case 3:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.mapstyle));

                break;

            default:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
        }

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);


        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        View locationButton = ((View) binding.map.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        rlp.setMargins(0, 16, 16, 0);


        CameraUpdate coordinate = CameraUpdateFactory.newLatLngZoom(new LatLng(32.6896245, 51.6002422), CAMERA_ZOOM);

        googleMap.animateCamera(coordinate);

        googleMap.setOnMapClickListener(latLng -> presenter.onMapSelected(latLng));

        presenter.onMapReady();
    }

    private void displayMarker(LatLng latLng, String title) {
        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_red));
        markerOptions.position(latLng);

        markerOptions.title(title);

        googleMap.addMarker(markerOptions);

    }

}
