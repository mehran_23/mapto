package com.selectmap.app.ui.activities.launcher

import androidx.lifecycle.ViewModel
import com.selectmap.app.model.GeneralRepository

class SplashViewModel(private val generalRepository: GeneralRepository) : ViewModel() {

    fun setFirstArrivalIfFlase() {
        if (!isFirstArrival())
            setFirstArrival(true)
    }

    fun isFirstArrival(): Boolean {
        return generalRepository.isFirstArrival()
    }



    fun setFirstArrival(isFirstArrival: Boolean) {
        generalRepository.setFirstArrival(isFirstArrival)
    }
}