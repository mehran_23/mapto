package com.selectmap.app.ui.dialogCenter;

import android.app.Activity;
import android.app.ProgressDialog;

import com.selectmap.app.R;


public class WaitProgressDialog {
    public static ProgressDialog waitProgressDialog;

    public static void showProgressDialog(Activity activity) {

        waitProgressDialog = new ProgressDialog(activity);

        if (!waitProgressDialog.isShowing()) {
            waitProgressDialog.setMessage(activity.getString(R.string.label_please_wait));
            waitProgressDialog.setCancelable(false);
            waitProgressDialog.show();
        }
    }

    public static void hideProgressDialog() {
        try {
            if (waitProgressDialog != null) {
                if (waitProgressDialog.isShowing()) {
                    waitProgressDialog.dismiss();
                }
            }

        } catch (Exception ignored) {
        }

    }

    public interface ProgressDialogStatus {
        void showProgressDialog();

        void hideProgressDialog();
    }
}
