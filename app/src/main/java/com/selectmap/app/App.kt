package com.selectmap.app;

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.selectmap.app.data.databases.preferences.SharePref
import com.selectmap.app.data.databases.preferences.SharedPrefProvider
import com.selectmap.app.model.GeneralRepository
import com.selectmap.app.ui.activities.launcher.SplashViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class App : Application(), KodeinAware {

    companion object {
        private var instance: App? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }

        private const val TAG = "DemoApplication"
    }

    init {
        instance = this
    }

    override val kodein = Kodein.lazy {
        import(androidXModule(this@App))

//        utils
        bind() from singleton { SharedPrefProvider() }

//        repositories
        bind() from singleton { GeneralRepository(instance()) }

//        factories
        bind() from provider { SplashViewModelFactory(instance()) }

    }

    fun closeApp() {
        System.exit(0)
    }

    fun userLogout() {
        val editor = SharePref.getmSharedPreferences().edit()
        editor.clear()
        editor.apply()
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showKeyboard() {
        val imm = instance!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    val versionCode: String?
        get() = try {
            val pInfo = instance!!.packageManager.getPackageInfo(instance!!.packageName, 0)
            pInfo.versionCode.toString() + ""
        } catch (e: PackageManager.NameNotFoundException) {
            null
        }

    val isReady: Boolean
        get() = instance != null
}