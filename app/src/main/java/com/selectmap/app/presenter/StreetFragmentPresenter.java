package com.selectmap.app.presenter;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.selectmap.app.data.databases.preferences.SharePref;
import com.selectmap.app.inteface.StreetFragmentContract;
import com.selectmap.app.model.LocationModel;
import com.selectmap.app.ui.fragments.MainFragment;

public class StreetFragmentPresenter implements StreetFragmentContract.Presenter {

    private StreetFragmentContract.View view;
    private LocationModel locationModel;

    @Override
    public void attach(StreetFragmentContract.View view) {
        this.view = view;
        locationModel = new LocationModel(SharePref.Instance(), new Gson());

        locationModel.getLatLngMutableLiveData().observeForever(latLng -> onMapReady());
    }

    @Override
    public void detach() {
        view = null;
    }

    @Override
    public void onMapSelected(LatLng latLng) {

        view.showConfirmDialog(v -> {
            locationModel.saveLocation(latLng);
            view.showMessage("موقعیت با موفقیت ذخیره شد.");
            MainFragment.newInstance().changeCurrentFragment(3,true);
        });
    }

    @Override
    public void onMapReady() {
        view.displayFavoritLocation(locationModel.loadLocation());
    }
}
