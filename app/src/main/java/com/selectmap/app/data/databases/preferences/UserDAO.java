package com.selectmap.app.data.databases.preferences;


import com.google.gson.Gson;

public class UserDAO {
    private SharePref sharePref;
    private Gson gson;

    public UserDAO(SharePref sharePref, Gson gson) {
        this.sharePref = sharePref;
        this.gson = gson;
    }


    public interface UserDAOActions<T> {
        void onUserDAOTransActionSucceed(T object);

        void onUserDAOTransActionError(String message, T object);
    }
}
