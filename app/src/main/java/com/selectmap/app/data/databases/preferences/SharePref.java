package com.selectmap.app.data.databases.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.selectmap.app.App;

public final class SharePref {

    private static final String SHAREPREF_ACCESS_KEY = "share_pref_access_key";
    private static SharedPreferences mSharedPreferences;

    private static SharePref sharePref;

    private SharePref() {
    }

    public static SharePref Instance() {
        if (sharePref == null) {
            sharePref = new SharePref();
            mSharedPreferences = App.Companion.applicationContext().getSharedPreferences(SHAREPREF_ACCESS_KEY, Context.MODE_PRIVATE);
        }
        return sharePref;

    }

    public void put(String key, String value) {
        mSharedPreferences.edit().putString(key, value).commit();
    }

    public void put(String key, int value) {
        mSharedPreferences.edit().putInt(key, value).apply();
    }

    public void put(String key, float value) {
        mSharedPreferences.edit().putFloat(key, value).apply();
    }

    public void put(String key, long value) {
        mSharedPreferences.edit().putLong(key, value).apply();
    }

    public void put(String key, boolean value) {
        mSharedPreferences.edit().putBoolean(key, value).apply();
    }

    public String get(String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    public Integer get(String key, int defaultValue) {
        return mSharedPreferences.getInt(key, defaultValue);
    }

    public Float get(String key, float defaultValue) {
        return mSharedPreferences.getFloat(key, defaultValue);
    }

    public Boolean get(String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    public long get(String key, long defaultValue) {
        return mSharedPreferences.getLong(key, defaultValue);
    }

    public void deleteSavedData(String key) {
        mSharedPreferences.edit().remove(key).apply();
    }

    public static SharedPreferences getmSharedPreferences() {
        return mSharedPreferences;
    }


    public void deleteData() {
        SharedPreferences.Editor editor = SharePref.getmSharedPreferences().edit();
        editor.clear();
        editor.commit();
    }

}
