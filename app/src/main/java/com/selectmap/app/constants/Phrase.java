package com.selectmap.app.constants;


import android.os.Environment;

public class Phrase {

    //    KEYS
    public static final String SHAREPREF_ACCESS_KEY = "share_pref_access_key";
    public static final String DEFAULT_FRAGMENT_KEY = "default_fragment";

    //    END POINTS
    //    public static final String BASE_URL = BuildConfig.BASE_URL;
    //    public static final String BASE_DOWNLOAD_URL = BuildConfig.BASE_DOWNLOAD_URL;
    //    paths
    public static String FILES_FOLDER = Environment.getExternalStorageDirectory() + "/mrtApp/file";
    public static String IMAGE_FOLDER = FILES_FOLDER + "/image";
    public static String VIDEO_FOLDER = FILES_FOLDER + "/video";
    public static String AUDIO_FOLDER = FILES_FOLDER + "/audio";
    public static String DOCUMENT_FOLDER = FILES_FOLDER + "/document";
    public static String TEMP_FOLDER = FILES_FOLDER + "/temp";

    public static final int id_bottommenu_bookmark = 1001;
    public static final int id_bottommenu_normal = 1002;
    public static final int id_bottommenu_hybrid = 1003;
    public static final int id_bottommenu_satelite = 1004;
}
