package com.selectmap.app.inteface;

import com.google.android.gms.maps.model.LatLng;
import com.selectmap.app.ui.dialogCenter.WaitProgressDialog;

import java.util.List;

public interface LocationContract {
    interface View extends WaitProgressDialog.ProgressDialogStatus {

    }

    interface Presenter extends BasePresenter<StreetFragmentContract.View> {

    }

    interface Model {

        void saveLocation(LatLng latLng);

        List<LatLng> loadLocation();
    }
}
