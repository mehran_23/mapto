package com.selectmap.app.inteface;

public interface BasePresenter<T> {

    void attach(T view);

    void detach();

}
