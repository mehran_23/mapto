package com.selectmap.app.inteface;


import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.selectmap.app.ui.dialogCenter.WaitProgressDialog;

import java.util.List;

public interface StreetFragmentContract {

    interface View extends WaitProgressDialog.ProgressDialogStatus {
        void replaceHomeFragment();

        void showMessage(String message);

        void showMessage(int message);

        void onFinish();

        void showConfirmDialog(android.view.View.OnClickListener onClickListener);

        void displayFavoritLocation(List<LatLng> loadLocation);
    }

    interface Presenter extends BasePresenter<View> {

        void onMapSelected(LatLng latLng);

        void onMapReady();
    }

    interface Model {


    }
}