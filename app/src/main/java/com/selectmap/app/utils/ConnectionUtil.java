package com.selectmap.app.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.selectmap.app.constants.ConnectionEnum;
import com.selectmap.app.ui.dialogCenter.DialogCollection;
import com.selectmap.app.ui.dialogCenter.QuickToast;


public class ConnectionUtil {

    public static boolean isOnline(Activity activity) {

        ConnectivityManager connMgr = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static boolean isOnlineWithAlarm(Activity activity, ConnectionEnum typeAlarm) {
        ConnectivityManager connMgr = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected()) {
            switch (typeAlarm) {
                case CONNECTION_DIALOG:
                    DialogCollection.showConnectionErrorDialog(activity);
                    break;

                case CONNECTION_TOAST:
                    QuickToast.ErrorInternetConnection();
                    break;

                case CONNECTION_TOAST_FINISH:
                    QuickToast.ErrorInternetConnection();
                    activity.finish();
                    break;
            }
            return false;
        }
        return true;
    }

}
