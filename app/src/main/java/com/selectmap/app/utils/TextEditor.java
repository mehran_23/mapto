package com.selectmap.app.utils;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.selectmap.app.App;
import com.selectmap.app.R;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class TextEditor {

    public static String commaSeprator(int wallet) {
        return NumberFormat.getInstance().format(wallet);
    }

    public static Spannable changeTextColor(String text, int colorId) {
        Spannable spannable = new SpannableString(text);

        spannable.setSpan(new ForegroundColorSpan(App.Companion.applicationContext().getResources().getColor(colorId)), 0
                , text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public static void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), color), PorterDuff.Mode.SRC_IN));
            }
        }
    }


    public static String twoDigits(int number) {
        if (String.valueOf(number).length() == 1) return "0" + number;

        return number + "";
    }

    public static String convertSecondToMinutes(Context context, int secondes) {
        return toPersianNumber(context, twoDigits(secondes / 60) + ":" + twoDigits(secondes % 60));
    }

    public static String toPersianNumber(Context context, String number) {
        char[] mPersianNumbers = null;
        if (context != null)
            mPersianNumbers = context.getString(R.string.persian_numbers).toCharArray();

        if (mPersianNumbers == null)
            return "";

        int arraySize = mPersianNumbers.length;
        if (arraySize != 0) {
            return number
                    .replace('0', mPersianNumbers[0])
                    .replace('1', mPersianNumbers[1])
                    .replace('2', mPersianNumbers[2])
                    .replace('3', mPersianNumbers[3])
                    .replace('4', mPersianNumbers[4])
                    .replace('5', mPersianNumbers[5])
                    .replace('6', mPersianNumbers[6])
                    .replace('7', mPersianNumbers[7])
                    .replace('8', mPersianNumbers[8])
                    .replace('9', mPersianNumbers[9]);
        }
        return null;
    }

    public static Typeface getFontIransans() {
        return Typeface.createFromAsset(App.Companion.applicationContext().getAssets(), "font/yekan.ttf");
    }

    public static String convertToStandardMobileNumber(String number) {
        switch (number.length()) {
            case 10:
                return "98" + number;
            case 11:
                if (number.charAt(0) == '0') {
                    number = number.substring(1);
                    return "98" + number;
                }
                break;
            case 13:
                if (number.substring(0, 2).equals("98") && number.charAt(3) == '0') {
                    number = number.substring(3);
                    return "98" + number;
                }
                if (number.substring(0, 3).equals("98") && number.charAt(3) != '0') {
                    return number;
                }
                if (number.substring(0, 3).equals("098") && number.charAt(3) != '0') {
                    number = number.substring(3);
                    return "98" + number;
                }
                break;
            case 12:
                if (number.substring(0, 2).equals("98") && number.charAt(3) != '0') {
                    number = number.substring(2);
                    return "98" + number;
                }
                break;
            case 15:
                if (number.substring(0, 4).equals("0098") && number.charAt(4) == '0') {
                    number = number.substring(5);
                    return "98" + number;
                }
                break;
            case 14:
                if (number.substring(0, 4).equals("0098") && number.charAt(4) != '0') {
                    number = number.substring(4);
                    return "98" + number;
                }
                if (number.substring(0, 3).equals("+98") && number.charAt(3) == '0') {
                    number = number.substring(4);
                    return "98" + number;
                }
                if (number.substring(0, 3).equals("098") && number.charAt(3) == '0') {
                    number = number.substring(4);
                    return "98" + number;
                }
                break;
        }
        return number.trim();
    }

    public static String timestampTimeFormat(long timestamp, String format, String local) {
        return new SimpleDateFormat(format, new Locale(local)).format(new java.util.Date(timestamp));
    }

    public static String displayTimestampTimeFormat(long timestamp,String local) {
        SimpleDateFormat monthDate = new SimpleDateFormat("MMM", new Locale(local));
        return  timestampTimeFormat(timestamp, "dd", "fa")
        +" "+monthDate.format(timestamp) + " " + timestampTimeFormat(timestamp, "HH:mm", "fa");
    }

    public static String getDurationString(int seconds) {

        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        return  twoDigitString(seconds)+ " : " + twoDigitString(minutes) + " : " + twoDigitString(hours);
    }

    public static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }
}
