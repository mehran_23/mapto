package com.selectmap.app.utils;

import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class PlayServicesUtil {

    public static boolean isAvailable(Context context) {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        switch (status) {

            case ConnectionResult.SUCCESS:
                return true;

            case ConnectionResult.SERVICE_DISABLED:
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;

            default:
                break;
        }
        return false;
    }
}
